FROM python:3.11.2-slim-buster
WORKDIR /ac4
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["flask", "run", "--host=0.0.0.0"]
