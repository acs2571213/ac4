import unittest


from app import somar


class TestApp(unittest.TestCase):
    def teste_resultado_soma(self):
        self.assertEqual(somar(5, 4), 9)
        self.assertEqual(somar(4, 4), 8)
        self.assertEqual(somar(3, 4), 7)
        self.assertEqual(somar(2, 1), 3)
